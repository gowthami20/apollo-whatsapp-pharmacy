import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    header: {
      background: '#fff',
      padding: '10px 20px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    logo: {
      padding: '0 30px 0 0',
      borderRight: '1px solid rgba(0,0,0,0.1)',
    }
  }),
);
export default function ButtonAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.header}>
          <Link href="#" className={classes.logo}><img src={require('../images/logo.png')} alt="Logo" /></Link>
          <Link href="#"><img src={require('../images/profile.png')} alt="profile" /></Link>
        </Toolbar>
      </AppBar>
    </div>
  );  
}