import React from 'react';
import { createStyles, makeStyles, Theme, withStyles, fade } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Grid from '@material-ui/core/Grid';
import AssignmentIcon from '@material-ui/icons/Assignment';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import InputBase from '@material-ui/core/InputBase';
import FormControl from '@material-ui/core/FormControl';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles((theme: Theme) =>{
return {
    user: {},
    sectionHeader: {
        padding: '10px',
        background: '#fff',
        borderRadius: '5px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: '0 0 20px',
        '& h2': {
            fontSize: '17px',
            textTransform: 'uppercase',
            color: '#02475b',
            fontWeight: 'bold',
            margin: '0 0 0 10px',
        },
    },
    grid: {},
    paperCard: {
        borderRadius: 5,
        padding: 15,
        '& h3': {
            fontSize: 16,
            padding: '0 0 10px',
            borderBottom: '1px solid rgba(2, 71, 91, .71)',
            textTransform: 'uppercase',
            color: '#02475b',
            fontWeight: 'bold',
            margin: '0 0 15px'
        }
    },
    detailsContent: {
        padding: 12,
        background: '#f7f8f5',
        borderRadius: 5,
    },
    uploadDetails: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: '0 0 20px',
        '& p': {
            fontSize: 16,
            color: '#02475b',
        },
    },
    secondaryBtn: {
        background: '#fcb716',
        color: '#fff',
        fontSize: 13,
        borderRadius: 10,
        textTransform: 'uppercase',
        boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.2)'
    },
    formControl: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: '0 0 10px',
        '& *': {
            flex: '1 0 auto'
        },
        '& label': {
            fontSize: 14,
            fontWeight: 'bold',
            color: '#02475b',
            position: 'static',
            transform: 'none',
        },
        // '& input': {
        //     width: 120,
        //     borderBottom: '1px solid rgba(2, 71, 91, 0.3)',
        //     fontSize: 16,
        //     padding: '0 10px 10px',
        // }
    },
    paper: {
        padding: 10,
        fontSize: 12,
        color: '#02475b'
    },
    heading: {
        fontSize: '14px',
        textTransform: 'uppercase',
        color: '#02475b',
        fontWeight: 'bold',
        margin: '10px 0'
    },
    modal: {}
}
});

const Input = withStyles((theme: Theme) =>
  createStyles({
    input: {
      position: 'relative',
      borderBottom: '2px solid rgba(2, 71, 91, 0.3)',
      fontSize: 14,
      width: '120px',
      padding: '0 5px 5px',
    },
  }),
)(InputBase);
export default function NewUser() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };
    return (
        <div className={classes.user}>
            <div className={classes.sectionHeader}>
                <Link href="#"><ArrowBackIcon></ArrowBackIcon></Link> 
                <Typography variant="h2" component="h2">
                    Backend Punching Tool
                </Typography>
            </div>
             <Grid container className={classes.grid} spacing={2}>
                <Grid item xs={4}>
                    <Paper className={classes.paperCard}>
                        <Typography variant="h3" component="h3">
                            User Details
                        </Typography>
                        <div className={classes.detailsContent}>
                            <form className="form">
                            <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap- input">
                                Phone Number-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                                </FormControl>
                                <Paper className={classes.paper}>New user to Apollo 247. Add address and place 
order to create user.</Paper>
<FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap-input">
                                Customer Name-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                                </FormControl>
                                 <Typography component="h4" className={classes.heading}> Address</Typography>
                                <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap-input">
                                Address 1-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                                </FormControl>
                                <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap-input">
                                Address 2-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                                </FormControl>
                                <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap-input">
                                Pincode-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                                </FormControl>
                                <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap-input">
                                City-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                                </FormControl>
                                <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap-input">
                                State-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                                </FormControl>
                                 <Typography component="h4" className={classes.heading}>Address Type</Typography>

                            </form>
                        </div>
                    </Paper>
                </Grid>
                <Grid item xs={4}>
                    <Paper className={classes.paperCard}>
                    <Typography variant="h3" component="h3">
                            Upload User Prescription
                        </Typography>
                        <div className={classes.detailsContent}>
                            <div className={classes.uploadDetails}>
                                <Typography component="p">
                                    PDF/JPEG of prescription can be uploaded.
                                </Typography>
                                <AssignmentIcon></AssignmentIcon>
                            </div>    
                            <Button variant="contained" color="secondary" className={classes.secondaryBtn}>
                                Upload Prescription
                            </Button>
                        </div>
                    </Paper>
                </Grid>
                <Grid item xs={4}>
                    <Paper className={classes.paperCard}>
                    <Typography variant="h3" component="h3">
                            Users Comments
                        </Typography>
                        <div className={classes.detailsContent}>
                        <form>
                             <Typography component="h4" className={classes.heading}>Items</Typography>
                            <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap- input">
                                Items No1-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap- input">
                                Items No2-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                            </FormControl>
                            <Link >Add Items</Link>
                            <FormControl className={classes.formControl}>
                                <InputLabel shrink htmlFor="bootstrap- input">
                                Coupon Code-
                                </InputLabel>
                                <Input defaultValue=" " id="bootstrap-input" />
                            </FormControl>
                            <FormControl className={classes.formControl} >
                                <InputLabel shrink htmlFor="bootstrap- input">
                                 Any Other Comments-
                                </InputLabel>
                                </FormControl>
                                <FormControl className={classes.formControl} >
                                <Input defaultValue=" " id="bootstrap-input" />
                            </FormControl>
                        </form>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}
