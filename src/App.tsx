import React from 'react';
// import logo from './logo.svg';
import Header from './components/header';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import NewUser from './components/newUser';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        app: {
            height: '100%'
        },
        mainContent: {
            padding: 20,
            background: '#f7f8f5',
            height: '100%'
        },
        container: {
            width: 1064,
            margin: '0 auto'
        }
    }),
);

function App() {
    const classes = useStyles();

    return (
        <div className={classes.app}>
            <div className={classes.container}>
            <Header></Header>
            <div className={classes.mainContent}>
                <NewUser></NewUser>
            </div>
            </div>
        </div>
    );
}

export default App;
